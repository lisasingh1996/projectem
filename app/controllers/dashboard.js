import Controller from '@ember/controller';
import { computed } from '@ember/object';
import { sort } from '@ember/object/computed';


export default Controller.extend({
  sortProperty: 'budget:desc',

  customersSortProps: computed('sortProperty', function() {
    return [this.sortProperty];
  }),
  totalBudget:computed('sortProperty', function() {
    let sum = 0;
    this.get('model').forEach(customer => {
       sum += parseInt(customer.get('budget'));
    });
    return sum;
  }),
  sortedCustomers: sort('model', 'customersSortProps')

});


