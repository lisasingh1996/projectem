import Controller from '@ember/controller';
import { computed } from '@ember/object';
import { sort } from '@ember/object/computed';


export default Controller.extend({
  sortProperty: 'firstName',
  search:'hi',
  result:'waiting',
  fullName: computed('firstName', 'lastName', function(){
      return this.firstName+ ' ' + this.lastName;
  }),
  customersSortProps: computed('sortProperty', function() {
    return [this.sortProperty];
  }),
  totalBudget:computed('sortProperty', function() {
    let sum = 0;
    this.get('model').forEach(customer => {
       sum += parseInt(customer.get('budget'));
    });
    return sum;
  }),
  sortedCustomers: sort('model', 'customersSortProps'),

  actions:{
    searchCustomer:function() {
      let searchBy= this.get('searchBy');

      this.set('search',searchBy);
      this.get('model').forEach(customer=>{
        if(customer.get('firstName')==searchBy || customer.get('company')==searchBy || customer.get('project')==searchBy){
          this.set('result',customer);
        }
      });
      this.set('sortedCustomers',sort(this.result,'customersSortProps'));
      // this.get('model').query('customer', {
      //   filter: {
      //     project: searchBy
      //   }
      // }).then(function() {
      //   this.set('result','true');
      // });
    }
  }





});


